ONNX được sử dụng để chuyển đổi giữa các Framewok, Platform.
Ví dụ, một model được huấn luyện với Pytorch framework. Thông thường để sử dụng nó với Tensorflow, ta sẽ phải train một model tương tự trong Tensorflow và sử dụng.
Tuy nhiên, với ONNX ta có thể convert Pytorch model thành ONNX model và convert ONNX model thành Tensorflow.

Để có thể visualize kiến trúc một model, ta có thể sử dụng [Netron](https://github.com/lutzroeder/netron).

ONNX runtime được sử dụng để tạo session và sau đó sử dụng onnx model để infer trong phiên đó. Chức năng của nó giống như Java Runtime Environment để chạy code Java.

Bài hướng dẫn này dựa trên trang thenewstack.io:

[Phần 1: Giới thiệu ONNX và ONNX Runtime](https://thenewstack.io/open-neural-network-exchange-brings-interoperability-to-machine-learning-frameworks/)

[Phần 2: Sử dụng pretrained onnx model để thực hiện inference](https://thenewstack.io/tutorial-using-a-pre-trained-onnx-model-for-inferencing/)

[Phần 3: Huấn luyện mô hình sử dụng Pytorch và Export nó thành ONNX](https://thenewstack.io/tutorial-train-a-deep-learning-model-in-pytorch-and-export-it-to-onnx/)

[Phần 4: Convert ONNX model ở phần 3 sang Tensorflow model và thực hiện Inference](https://thenewstack.io/tutorial-import-an-onnx-model-into-tensorflow-for-inference/)

