# https://pytorch.org/tutorials/beginner/blitz/autograd_tutorial.html

import torch

a = torch.tensor([2., 3.], requires_grad=True)
b = torch.tensor([6., 4.], requires_grad=True)

Q = 3*a**3 - b**2

# external_grad = torch.tensor([1., 1.])
# Q.backward(gradient=external_grad)
Q.sum().backward()
print(a.grad, b.grad)

print(9*a**2 == a.grad)
print(-2*b == b.grad)
