# https://pytorch.org/tutorials/beginner/basics/transforms_tutorial.html

import torch
from torchvision import datasets
from torchvision.transforms import ToTensor, Lambda


# label y is an integer. So we use Lambda as the follow to convert the label into a one-hot vector form.

ds = datasets.FashionMNIST(
    root="data",
    train=True,
    download=True,
    transform=ToTensor(),
    target_transform=Lambda(lambda y: torch.zeros(10, dtype=torch.float).scatter_(0, torch.tensor(y), value=1))
)

tranf = Lambda(lambda y: torch.zeros(10, dtype=torch.float).scatter_(0, torch.tensor(y), value=1))

rs = tranf(1)
print(rs)